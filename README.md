# Yoyo's custom VIM configuration

My private VIM setup, strongly based on [spf13-vim](https://github.com/spf13/spf13-vim)

Bundles are hard copied into this repo (no version management).

Use on your own risk ;)
