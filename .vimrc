" Highly inspired by spf13-vim (http://spf13.com)
"
" vim: set foldmarker={,} foldlevel=0 foldmethod=marker spell:

let mapleader = ','

" Environment {
    " Basics {
        set nocompatible        " must be first line
        if has ("unix") && "Darwin" != system("echo -n \"$(uname)\"")
          " on Linux use + register for copy-paste
          set clipboard=unnamedplus
        else
          " one mac and windows, use * register for copy-paste
          set clipboard=unnamed
        endif
    " }
    " Windows Compatible {
        " On Windows, also use '.vim' instead of 'vimfiles'; this makes synchronization
        " across (heterogeneous) systems easier.
        if has('win32') || has('win64')
          set runtimepath=$HOME/.vim,$VIM/vimfiles,$VIMRUNTIME,$VIM/vimfiles/after,$HOME/.vim/after
        endif
    " }
    " Filetype Workarounds {
        " http://github.com/cakebaker/scss-syntax.vim
        autocmd BufNewFile,BufNewFile *.scss set filetype=scss
        autocmd BufNewFile,BufNewFile *.sass set filetype=scss
        autocmd BufNewFile,BufNewFile *.less set filetype=scss
    " }
    " Setup Bundle Support {
    " The next three lines ensure that the ~/.vim/bundle/ system works
        filetype on
        filetype off
        set rtp+=~/.vim/bundle/vundle
        call vundle#rc()
    " }
" }
" Bundles {
  " Inspired by .vimrc.bundles (spf13-vim)
  " Blade {
    Bundle 'xsbeats/vim-blade'
  " }
  " Deps {
    Bundle 'gmarik/vundle'
    Bundle 'MarcWeber/vim-addon-mw-utils'
    Bundle 'tomtom/tlib_vim'
    " if executable('ack-grep')
    "     let g:ackprg="ack-grep -H --nocolor --nogroup --column"
    "     Bundle 'mileszs/ack.vim'
    " elseif executable('ack')
    "     Bundle 'mileszs/ack.vim'
    " endif
  " }
  " General {
    " NERDtree {
      Bundle 'scrooloose/nerdtree'

      map <C-e> :NERDTreeToggle<CR>:NERDTreeMirror<CR>
      map <leader>e :NERDTreeFind<CR>
      nmap <leader>nt :NERDTreeFind<CR>

      let g:NERDShutUp=1
      let NERDTreeShowBookmarks=1
      let NERDTreeIgnore=['\.pyc', '\~$', '\.swo$', '\.swp$', '\.git', '\.hg', '\.svn', '\.bzr']
      let NERDTreeChDirMode=2
      let NERDTreeQuitOnOpen=1
      let NERDTreeMouseMode=2
      let NERDTreeShowHidden=1
      let NERDTreeKeepTreeInNewTab=1
      let g:nerdtree_tabs_open_on_gui_startup=0

      " Overrides
      let g:NERDTreeQuitOnOpen=0
      let g:NERDTreeWinSize=35
      let g:NERDTreeCasadeOpenSingleChildDir=1
      let g:NERDTreeChDirMode=1
      let g:nerdtree_tabs_open_on_gui_startup=0
      let g:NERDTreeMinimalUI=0

    " }
    " AutoCloseTag {
      " Bundle 'AutoClose'
      " Make it so AutoCloseTag works for xml and xhtml files as well
      " au FileType xhtml,xml ru ftplugin/html/autoclosetag.vim
      " nmap <Leader>ac <Plug>ToggleAutoCloseMappings
    " }
    " Buffer explorer {
      Bundle 'corntrace/bufexplorer'
      nmap <leader>b :BufExplorer<CR>
    " }
    " ctrlp {
      Bundle 'kien/ctrlp.vim'

      let g:ctrlp_working_path_mode = 2
      nnoremap <silent> <D-t> :CtrlP<CR>
      nnoremap <silent> <D-r> :CtrlPMRU<CR>
      let g:ctrlp_custom_ignore = {
          \ 'dir':  '\.git$\|\.hg$\|\.svn$',
          \ 'file': '\.exe$\|\.so$\|\.dll$' }
    " }
    " UndoTree {
      " Bundle 'mbbill/undotree'
      " nnoremap <Leader>u :UndotreeToggle<CR>
    " }
    " indent_guides {
      " Bundle 'nathanaelkane/vim-indent-guides'
      " if !exists('g:spf13_no_indent_guides_autocolor')
      "     let g:indent_guides_auto_colors = 1
      " else
      "     " for some colorscheme ,autocolor will not work,like 'desert','ir_black'.
      "     autocmd VimEnter,Colorscheme * :hi IndentGuidesOdd  guibg=#212121   ctermbg=3
      "     autocmd VimEnter,Colorscheme * :hi IndentGuidesEven guibg=#404040 ctermbg=4
      " endif

      " set ts=2 sw=2 et
      " let g:indent_guides_start_level = 2
      " let g:indent_guides_guide_size = 1
      " let g:indent_guides_enable_on_vim_startup = 1
    " }
    Bundle 'spf13/vim-colors'
    " Bundle 'flazz/vim-colorschemes'
    " Bundle 'tpope/vim-surround'
    " Solarized Colorscheme {
      " Bundle 'altercation/vim-colors-solarized'

      " let g:solarized_termcolors=256
      " color solarized                 " load a colorscheme

      " let g:solarized_termtrans=1
      " let g:solarized_contrast="high"
      " let g:solarized_visibility="high"
    " }
    " Bundle 'vim-scripts/sessionman.vim'
    " Bundle 'matchit.zip'
    " Bundle 'Lokaltog/vim-powerline'
    " Bundle 'Lokaltog/vim-easymotion'
    " Bundle 'godlygeek/csapprox'
    " Bundle 'jistr/vim-nerdtree-tabs'
    " Bundle 'myusuf3/numbers.vim'
    Bundle 'imeos/vim-irblack'

  " }
  " General Programming {
    " Tabular {
      " Bundle 'godlygeek/tabular'
      " if exists(":Tabularize")
      "   nmap <Leader>a= :Tabularize /=<CR>
      "   vmap <Leader>a= :Tabularize /=<CR>
      "   nmap <Leader>a: :Tabularize /:<CR>
      "   vmap <Leader>a: :Tabularize /:<CR>
      "   nmap <Leader>a:: :Tabularize /:\zs<CR>
      "   vmap <Leader>a:: :Tabularize /:\zs<CR>
      "   nmap <Leader>a, :Tabularize /,<CR>
      "   vmap <Leader>a, :Tabularize /,<CR>
      "   nmap <Leader>a<Bar> :Tabularize /<Bar><CR>
      "   vmap <Leader>a<Bar> :Tabularize /<Bar><CR>

      "   " The following function automatically aligns when typing a
      "   " supported character
      "   inoremap <silent> <Bar>   <Bar><Esc>:call <SID>align()<CR>a

      "   function! s:align()
      "       let p = '^\s*|\s.*\s|\s*$'
      "       if exists(':Tabularize') && getline('.') =~# '^\s*|' && (getline(line('.')-1) =~# p || getline(line('.')+1) =~# p)
      "           let column = strlen(substitute(getline('.')[0:col('.')],'[^|]','','g'))
      "           let position = strlen(matchstr(getline('.')[0:col('.')],'.*|\s*\zs.*'))
      "           Tabularize/|/l1
      "           normal! 0
      "           call search(repeat('[^|]*|',column).'\s\{-\}'.repeat('.',position),'ce',line('.'))
      "       endif
      "   endfunction

      " endif
    " }
    " TagBar {
      " if executable('ctags')
        " Bundle 'majutsushi/tagbar'
      " endif

      " nnoremap <silent> <leader>tt :TagbarToggle<CR>
    "}
    " Fugitive {
      Bundle 'tpope/vim-fugitive'

      nnoremap <silent> <leader>gs :Gstatus<CR>
      nnoremap <silent> <leader>gd :Gdiff<CR>
      nnoremap <silent> <leader>gc :Gcommit<CR>
      nnoremap <silent> <leader>gb :Gblame<CR>
      nnoremap <silent> <leader>gl :Glog<CR>
      nnoremap <silent> <leader>gp :Git push<CR>
    "}
    " NERD Commenter {
      filetype plugin on
      let g:NERDRemoveExtraSpaces=0
      let g:NERDSpaceDelims=1
      Bundle 'scrooloose/nerdcommenter'
    " }
    " Pick one of the checksyntax, jslint, or syntastic
    " Bundle 'scrooloose/syntastic'
    " Bundle 'mattn/webapi-vim'
    " Bundle 'mattn/gist-vim'
  " }
  " Snippets & AutoComplete {
    " Snipmate {
      " Bundle 'garbas/vim-snipmate'
      " Bundle 'honza/snipmate-snippets'
      " " Source support_function.vim to support snipmate-snippets.
      " if filereadable(expand("~/.vim/bundle/snipmate-snippets/snippets/support_functions.vim"))
      "     source ~/.vim/bundle/snipmate-snippets/snippets/support_functions.vim
      " endif
      "
      " Setting the author var
      " If forking, please overwrite in your .vimrc.local file
      " let g:snips_author = 'Steve Francia <steve.francia@gmail.com>'
    " }
    " OR neocomplcache {
        " Bundle 'Shougo/neocomplcache'
        " Bundle 'Shougo/neocomplcache-snippets-complete'
        " Bundle 'honza/snipmate-snippets'

        " let g:neocomplcache_enable_at_startup = 1
        " let g:neocomplcache_enable_camel_case_completion = 1
        " let g:neocomplcache_enable_smart_case = 1
        " let g:neocomplcache_enable_underbar_completion = 1
        " let g:neocomplcache_min_syntax_length = 3
        " let g:neocomplcache_enable_auto_delimiter = 1
        " let g:neocomplcache_max_list = 15
        " let g:neocomplcache_auto_completion_start_length = 3
        " let g:neocomplcache_force_overwrite_completefunc = 1
        " let g:neocomplcache_snippets_dir='~/.vim/bundle/snipmate-snippets/snippets'

        " " AutoComplPop like behavior.
        " let g:neocomplcache_enable_auto_select = 0

        " " SuperTab like snippets behavior.
        " imap  <silent><expr><tab>  neocomplcache#sources#snippets_complete#expandable() ? "\<plug>(neocomplcache_snippets_expand)" : (pumvisible() ? "\<c-e>" : "\<tab>")
        " smap  <tab>  <right><plug>(neocomplcache_snippets_jump) 

        " " Plugin key-mappings.
        " " Ctrl-k expands snippet & moves to next position
        " " <CR> chooses highlighted value
        " imap <C-k>     <Plug>(neocomplcache_snippets_expand)
        " smap <C-k>     <Plug>(neocomplcache_snippets_expand)
        " inoremap <expr><C-g>   neocomplcache#undo_completion()
        " inoremap <expr><C-l>   neocomplcache#complete_common_string()
        " inoremap <expr><CR>    neocomplcache#complete_common_string()


        " " <CR>: close popup
        " " <s-CR>: close popup and save indent.
        " inoremap <expr><s-CR> pumvisible() ? neocomplcache#close_popup()"\<CR>" : "\<CR>"
        " inoremap <expr><CR>  pumvisible() ? neocomplcache#close_popup() : "\<CR>"

        " " <TAB>: completion.
        " inoremap <expr><TAB>  pumvisible() ? "\<C-n>" : "\<TAB>"
        " inoremap <expr><s-TAB>  pumvisible() ? "\<C-p>" : "\<TAB>"

        " " <C-h>, <BS>: close popup and delete backword char.
        " inoremap <expr><BS> neocomplcache#smart_close_popup()."\<C-h>"
        " inoremap <expr><C-y>  neocomplcache#close_popup()

        " " Define keyword.
        " if !exists('g:neocomplcache_keyword_patterns')
        "   let g:neocomplcache_keyword_patterns = {}
        " endif
        " let g:neocomplcache_keyword_patterns['default'] = '\h\w*'

        " " Enable omni completion.
        " autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS
        " autocmd FileType html,markdown setlocal omnifunc=htmlcomplete#CompleteTags
        " autocmd FileType javascript setlocal omnifunc=javascriptcomplete#CompleteJS
        " autocmd FileType python setlocal omnifunc=pythoncomplete#Complete
        " autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags
        " autocmd FileType ruby setlocal omnifunc=rubycomplete#Complete

        " " Enable heavy omni completion.
        " if !exists('g:neocomplcache_omni_patterns')
        "     let g:neocomplcache_omni_patterns = {}
        " endif
        " let g:neocomplcache_omni_patterns.ruby = '[^. *\t]\.\h\w*\|\h\w*::'
        " let g:neocomplcache_omni_patterns.php = '[^. \t]->\h\w*\|\h\w*::'
        " let g:neocomplcache_omni_patterns.c = '\%(\.\|->\)\h\w*'
        " let g:neocomplcache_omni_patterns.cpp = '\h\w*\%(\.\|->\)\h\w*\|\h\w*::'

        " " For snippet_complete marker.
        " if has('conceal')
        "     set conceallevel=2 concealcursor=i
        " endif
    " }
  " }
  " PHP {
    " PIV {
      Bundle 'spf13/PIV'
      let g:DisableAutoPHPFolding = 0
      let g:PIVAutoClose = 0
    " }
  " }
  " Python {
    " Pick either python-mode or pyflakes & pydoc
    " Bundle 'klen/python-mode'
    " Bundle 'python.vim'
    " Bundle 'python_match.vim'
    " Bundle 'pythoncomplete'
    " PythonMode {
    " Disable if python support not present
      " if !has('python')
      "    let g:pymode = 1
      " endif
    " }
  " }
  " Javascript {
    " Bundle 'leshill/vim-json'
    " Bundle 'groenewege/vim-less'
    " Bundle 'pangloss/vim-javascript'
    " Bundle 'briancollins/vim-jst'
  " }
  " Java {
    " Bundle 'derekwyatt/vim-scala'
    " Bundle 'derekwyatt/vim-sbt'
  " }
  " HTML {
    " Bundle 'amirh/HTML-AutoCloseTag'
    " Bundle 'ChrisYip/Better-CSS-Syntax-for-Vim'
  " }
  " Ruby {
    " " Bundle 'tpope/vim-rails'
    " let g:rubycomplete_buffer_loading = 1
    " "let g:rubycomplete_classes_in_global = 1
    " "let g:rubycomplete_rails = 1
  " }
  " Misc {
    " Bundle 'tpope/vim-markdown'
    " Bundle 'spf13/vim-preview'
    " " Bundle 'tpope/vim-cucumber'
    " " Bundle 'Puppet-Syntax-Highlighting'
  " }
  " Twig {
    " Bundle 'beyondwords/vim-twig'
  " }
  " LargeFile {
    Bundle 'vim-scripts/LargeFile'
  " }
" }
" General {

  set background=dark         " Assume a dark background

  if !has('gui')
    set term=$TERM          " Make arrow and other keys work
  endif

  filetype plugin indent on   " Automatically detect file types.
  syntax on                   " syntax highlighting
  set mouse=a                 " automatically enable mouse usage
  scriptencoding utf-8

  " Most prefer to automatically switch to the current file directory when
  " a new buffer is opened; to prevent this behavior, add
  " let g:spf13_no_autochdir = 1 to your .vimrc.bundles.local file
  if !exists('g:spf13_no_autochdir')
    autocmd BufEnter * if bufname("") !~ "^\[A-Za-z0-9\]*://" | lcd %:p:h | endif
    " always switch to the current file directory.
  endif

  " set autowrite                  " automatically write a file when leaving a modified buffer
  set shortmess+=filmnrxoOtT      " abbrev. of messages (avoids 'hit enter')
  set viewoptions=folds,options,cursor,unix,slash " better unix / windows compatibility
  set virtualedit=onemore         " allow for cursor beyond last character
  set history=300                 " Store a ton of history (default is 20)
  " set spell                       " spell checking on
  set hidden                      " allow buffer switching without saving

  " Setting up the directories {
    set backup                      " backups are nice ...
    if has('persistent_undo')
      set undofile               "so is persistent undo ...
      set undolevels=500         "maximum number of changes that can be undone
      set undoreload=5000        "maximum number lines to save for undo on a buffer reload
    endif
  " }
  " Views {
    " Could use * rather than *.*, but I prefer to leave .files unsaved
    " au BufWinLeave *.* silent! mkview  "make vim save view (state) (folds, cursor, etc)
    " au BufWinEnter *.* silent! loadview "make vim load view (state) (folds, cursor, etc)
  " }
" }
" Vim UI {
  set tabpagemax=15               " only show 15 tabs
  set showmode                    " display the current mode

  set cursorline                  " highlight current line

  if has('cmdline_info')
    set ruler                   " show the ruler
    set rulerformat=%30(%=\:b%n%y%m%r%w\ %l,%c%V\ %P%) " a ruler on steroids
    set showcmd                 " show partial commands in status line and
  endif

  if has('statusline')
    set laststatus=2

    " Broken down into easily includeable segments
    set statusline=%<%f\    " Filename
    set statusline+=%w%h%m%r " Options
    set statusline+=%{fugitive#statusline()} "  Git
    set statusline+=\ [%{&ff}/%Y]            " filetype
    " set statusline+=\ [%{getcwd()}]          " current dir
    set statusline+=\ %p
    set statusline+=%=%-14.(%l,%c%V%)\ %p%%  " Right aligned file nav info
  endif

  set backspace=indent,eol,start  " backspace for dummies
  set linespace=0                 " No extra spaces between rows
  set nu                          " Line numbers on
  set showmatch                   " show matching brackets/parenthesis
  set incsearch                   " find as you type search
  set hlsearch                    " highlight search terms
  set winminheight=0              " windows can be 0 line high
  set ignorecase                  " case insensitive search
  set smartcase                   " case sensitive when uc present
  set wildmenu                    " show list instead of just completing
  set wildmode=list:longest,full  " command <Tab> completion, list matches, then longest common part, then all.
  set whichwrap=b,s,h,l,<,>,[,]   " backspace and cursor keys wrap to
  set scrolljump=5                " lines to scroll when cursor leaves screen
  set scrolloff=3                 " minimum lines to keep above and below cursor
  set foldenable                  " auto fold code
  set list
  set listchars=tab:,.,trail:.,extends:#,nbsp:. " Highlight problematic whitespace

" }
" Formatting {
  set nowrap
  set autoindent                  " indent at the same level of the previous line
  set shiftwidth=2                " use indents of 2 spaces
  set expandtab                   " tabs are spaces, not tabs
  set tabstop=2                   " an indentation every two columns
  set softtabstop=2               " let backspace delete indent
  "set matchpairs+=<:>                " match, to be used with %
  set pastetoggle=<F12>           " pastetoggle (sane indentation on pastes)
  "set comments=sl:/*,mb:*,elx:*/  " auto format comment blocks

  " Remove trailing whitespaces and ^M chars
  " autocmd FileType c,cpp,java,php,javascript,python,twig,xml,yml autocmd BufWritePre <buffer> :call setline(1,map(getline(1,"$"),'substitute(v:val,"\\s\\+$","","")'))

  autocmd BufNewFile,BufRead *.html.twig set filetype=html.twig
" }
" Key (re)Mappings {


    " Easier moving in tabs and windows
    map <C-J> <C-W>j<C-W>_
    map <C-K> <C-W>k<C-W>_
    map <C-L> <C-W>l<C-W>_
    map <C-H> <C-W>h<C-W>_

    " Wrapped lines goes down/up to next row, rather than next line in file.
    nnoremap j gj
    nnoremap k gk

    " The following two lines conflict with moving to top and bottom of the
    " screen
    " map <S-H> gT
    " map <S-L> gt

    " Stupid shift key fixes
    " if has("user_commands")
    "     command! -bang -nargs=* -complete=file E e<bang> <args>
    "     command! -bang -nargs=* -complete=file W w<bang> <args>
    "     command! -bang -nargs=* -complete=file Wq wq<bang> <args>
    "     command! -bang -nargs=* -complete=file WQ wq<bang> <args>
    "     command! -bang Wa wa<bang>
    "     command! -bang WA wa<bang>
    "     command! -bang Q q<bang>
    "     command! -bang QA qa<bang>
    "     command! -bang Qa qa<bang>
    " endif
    " cmap Tabe tabe

    " Yank from the cursor to the end of the line, to be consistent with C and D.
    " nnoremap Y y$

    """ Code folding options
    " nmap <leader>f0 :set foldlevel=0<CR>
    " nmap <leader>f1 :set foldlevel=1<CR>
    " nmap <leader>f2 :set foldlevel=2<CR>
    " nmap <leader>f3 :set foldlevel=3<CR>
    " nmap <leader>f4 :set foldlevel=4<CR>
    " nmap <leader>f5 :set foldlevel=5<CR>
    " nmap <leader>f6 :set foldlevel=6<CR>
    " nmap <leader>f7 :set foldlevel=7<CR>
    " nmap <leader>f8 :set foldlevel=8<CR>
    " nmap <leader>f9 :set foldlevel=9<CR>

    "clearing highlighted search
    nmap <silent> <leader>/ :nohlsearch<CR>

    " Shortcuts
    " Change Working Directory to that of the current file
    cmap cwd lcd %:p:h
    cmap cd. lcd %:p:h

    " visual shifting (does not exit Visual mode)
    vnoremap < <gv
    vnoremap > >gv

    " Fix home and end keybindings for screen, particularly on mac
    " - for some reason this fixes the arrow keys too. huh.
    map [F $
    " "imap [F $
    map [H g0
    " "imap [H g0

    " For when you forget to sudo.. Really Write the file.
    cmap w!! w !sudo tee % >/dev/null

    " Some helpers to edit mode
    " http://vimcasts.org/e/14
    cnoremap %% <C-R>=expand('%:h').'/'<cr>
    map <leader>ew :e %%
    map <leader>es :sp %%
    map <leader>ev :vsp %%
    map <leader>et :tabe %%

    " Adjust viewports to the same size
    " map <Leader>= <C-w>=

    " Easier horizontal scrolling
    map zl zL
    map zh zH
" }
" Plugins {
  " Misc {
    let b:match_ignorecase = 1
  " }
  " OmniComplete {
    " if has("autocmd") && exists("+omnifunc")
    "     autocmd Filetype *
    "         \if &omnifunc == "" |
    "         \setlocal omnifunc=syntaxcomplete#Complete |
    "         \endif
    " endif

    " hi Pmenu  guifg=#000000 guibg=#F8F8F8 ctermfg=black ctermbg=Lightgray
    " hi PmenuSbar  guifg=#8A95A7 guibg=#F8F8F8 gui=NONE ctermfg=darkcyan ctermbg=lightgray cterm=NONE
    " hi PmenuThumb  guifg=#F8F8F8 guibg=#8A95A7 gui=NONE ctermfg=lightgray ctermbg=darkcyan cterm=NONE

    " " some convenient mappings
    " inoremap <expr> <Esc>      pumvisible() ? "\<C-e>" : "\<Esc>"
    " inoremap <expr> <CR>       pumvisible() ? "\<C-y>" : "\<CR>"
    " inoremap <expr> <Down>     pumvisible() ? "\<C-n>" : "\<Down>"
    " inoremap <expr> <Up>       pumvisible() ? "\<C-p>" : "\<Up>"
    " inoremap <expr> <C-d>      pumvisible() ? "\<PageDown>\<C-p>\<C-n>" : "\<C-d>"
    " inoremap <expr> <C-u>      pumvisible() ? "\<PageUp>\<C-p>\<C-n>" : "\<C-u>"

    " " automatically open and close the popup menu / preview window
    " au CursorMovedI,InsertLeave * if pumvisible() == 0|silent! pclose|endif
    " set completeopt=menu,preview,longest
  " }
  " Ctags {
      set tags=./tags;/,~/.vimtags
  " }
  " Session List {
    " set sessionoptions=blank,buffers,curdir,folds,tabpages,winsize
    " nmap <leader>sl :SessionList<CR>
    " nmap <leader>ss :SessionSave<CR>
  " }
  " JSON {
    " nmap <leader>jt <Esc>:%!python -m json.tool<CR><Esc>:set filetype=json<CR>
  " }
  " PyMode {
    " let g:pymode_lint_checker = "pyflakes"
  " }
" }
" GUI Settings {
    " GVIM- (here instead of .gvimrc)
    if has('gui_running')
      set guioptions-=T           " remove the toolbar
      set lines=40                " 40 lines of text instead of 24,

      if has("gui_macvim")
        set guifont=Monaco:h13
      endif

      if has("gui_gtk2")
        set guifont=Andale\ Mono\ Regular\ 16,Menlo\ Regular\ 15,Consolas\ Regular\ 16,Courier\ New\ Regular\ 18
      else
        set guifont=Andale\ Mono\ Regular:h16,Menlo\ Regular:h15,Consolas\ Regular:h16,Courier\ New\ Regular:h18
      endif
      " if has('gui_macvim')
      "   set transparency=5          " Make the window slightly transparent
      " endif
    else
      if &term == 'xterm' || &term == 'screen'
        set t_Co=256                 " Enable 256 colors to stop the CSApprox warning and make xterm vim shine
      endif
      "set term=builtin_ansi       " Make arrow and other keys work
    endif
" }
" Functions {
  " InitializeDirectories {
    " function! InitializeDirectories()
    "     let separator = "."
    "     let parent = $HOME
    "     let prefix = '.vim'
    "     let dir_list = {
    "                 \ 'backup': 'backupdir',
    "                 \ 'views': 'viewdir',
    "                 \ 'swap': 'directory' }
    " 
    "     if has('persistent_undo')
    "         let dir_list['undo'] = 'undodir'
    "     endif
    " 
    "     for [dirname, settingname] in items(dir_list)
    "         let directory = parent . '/' . prefix . dirname . "/"
    "         if exists("*mkdir")
    "             if !isdirectory(directory)
    "                 call mkdir(directory)
    "             endif
    "         endif
    "         if !isdirectory(directory)
    "             echo "Warning: Unable to create backup directory: " . directory
    "             echo "Try: mkdir -p " . directory
    "         else
    "             let directory = substitute(directory, " ", "\\\\ ", "g")
    "             exec "set " . settingname . "=" . directory
    "         endif
    "     endfor
    " endfunction
    " call InitializeDirectories()
  " }
  " NERDTreeInitAsNeeded {
    " function! NERDTreeInitAsNeeded()
    "     redir => bufoutput
    "     buffers!
    "     redir END
    "     let idx = stridx(bufoutput, "NERD_tree")
    "     if idx > -1
    "         NERDTreeMirror
    "         NERDTreeFind
    "         wincmd l
    "     endif
    " endfunction
  " }
" }
" Local {
 
  color ir_black
  hi VertSplit        guifg=#61686c     guibg=#61686c     gui=NONE    ctermfg=darkgray    ctermbg=darkgray    cterm=NONE
  hi StatusLine       guifg=#FFFFFF     guibg=#61686c     gui=NONE    ctermfg=white       ctermbg=darkgray    cterm=NONE
  hi StatusLineNC     guifg=black       guibg=#61686c     gui=NONE    ctermfg=black       ctermbg=darkgray    cterm=NONE
  hi Folded           guifg=#8e9398     guibg=#2c2e31     gui=NONE      ctermfg=NONE        ctermbg=NONE        cterm=NONE

  set backupdir-=.
  set backupdir^=~/.vim/backup,~/tmp,/var/tmp,/tmp
  set directory=~/.vim/swap,~/tmp,/var/tmp,/tmp
  set undodir=~/.vim/undo,~/tmp,/var/tmp,/tmp
 
  if has('gui_running')
    if has('gui_macvim')
      set guifont=Andale\ Mono\ Regular:h13,Menlo\ Regular:h12,Consolas\ Regular:h13,Courier\ New\ Regular:h14
      set transparency=0
      " Try to make it fullscreen
      set lines=8888 columns=8888
    else
      set guifont=mono\ one\ 9
      set lines=999 columns=999
    endif
  endif
 
  "" Settings
 
  set nospell              " spell checking off
  set virtualedit-=onemore " do not allow for cursor beyond last character (deleting back with "x")
  set scrolljump=3         " lines to scroll when cursor leaves screen
  set gdefault             " /g domyslnie przy s///
 
  set shiftwidth=2  " use indents of 2 spaces
  set tabstop=2     " an indentation every four columns
  set softtabstop=2 " let backspace delete indent
  set ts=2 sw=2 et
  set backspace=2 " make backspace work normal
  set novisualbell " nie migaj
  set noerrorbells " nie hałasuj
 
  set listchars=tab:>>,trail:.,extends:#,nbsp:. " Highlight problematic whitespace
  hi SpecialKey guifg=#303030 guibg=#000000 ctermfg=7

  " if has("autocmd")
    " Drupal *.module and *.install files.
    " augroup module
    "   autocmd BufRead,BufNewFile *.php set filetype=php
    "   autocmd BufRead,BufNewFile *.module set filetype=php
    "   autocmd BufRead,BufNewFile *.install set filetype=php
    "   autocmd BufRead,BufNewFile *.test set filetype=php
    " augroup END

    " fun! <SID>abbrev_php()
      iab bench $start = (float)array_sum(explode(' ', microtime()));<CR><CR>echo $bench = round(((float)(array_sum(explode(' ', microtime()))-$start))*1000, 1).' ms';
      iab prr print '<pre>'.print_r(, true).'</pre>';<C-o>F,
    " endfun

    " augroup abbreviations
    "   autocmd!
    "   autocmd FileType php :call <SID>abbrev_php()
    " augroup END
  " endif
 
  " Kill line
  noremap <C-y> "_dd
  " space & shift-space in normal mode add line below or above
  noremap <S-Space> mzO<ESC>`z
  noremap <Space> mzo<ESC>`zh 
 
  " Move a line of text using alt
  nmap <M-j> mz:m+<cr>`z
  nmap <M-k> mz:m-2<cr>`z
  vmap <M-j> :m'>+<cr>`<my`>mzgv`yo`z
  vmap <M-k> :m'<-2<cr>`>my`<mzgv`yo`z
 
  map  <Leader>( :e ~/
 
  set noesckeys
  set ttimeout
  set ttimeoutlen=10
 
  " Update on F2
  nmap <f2> :update<cr>
  vmap <f2> <esc><f2>gv
  imap <f2> <c-o><f2> 

  nmap <f3> :w!<cr>
  vmap <f3> <esc><f2>gv
  imap <f3> <c-o><f2> 
 
  " Edit .vimrc with shift-F5 source it with F5
  map <silent> <S-F5> :e ~/.vimrc<CR>
  map <silent> <F5> :source ~/.vimrc<CR>
 
  map! <Leader>html <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"<CR>          "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><CR><ESC>I<html><CR><head><CR>  <title></title><CR><c-d></head><CR><body><CR><CR><CR></body><CR></html><ESC>2kO
  map! <Leader>css <link rel="stylesheet" type="text/css" href="css/.css" media="screen"/><ESC>F.i
  map! <Leader>ie <!--[if IE]><CR><![endif]--><ESC>
  map! <Leader>js <script type="text/javascript" src="js/.js"></script><ESC>F.i
  map! <Leader>utf <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 
  map! <Leader>" ""<Left>
  map! <Leader>' ''<Left>
  map! <Leader>( ()<Left>
  map! <Leader>$ ()<Left>
  map! <Leader>^ <C-o>A
  map! <Leader>{ {  }<Left><Left>
  map! <Leader>} <C-o><S-$> {}<Left><CR><CR><Up><Tab>
  map! <Leader>; <C-o><S-$>;

  " Changes in other files

     " Commented bundles in .vimrc.bundles
     " AutoClose (rozwiązało: Nie da się zrobić łatwo zrobić {{{ }}} (do foldowania))
     " nathanaelkane/vim-indent-guides

     " Commented in ~/.spf13-vim-3/.vim/bundle/PIV/ftplugin/php.vim
     " inoremap <buffer> <C-H> <ESC>:!phpm <C-R>=expand("<cword>")<CR><CR>
     " (nie) Rozwiązało: W PHP C-h (usuwanie linii) wykonuje :! phpm

     " Comment this two lines in .vimrc
     " imap [F $
     " imap [H g0

  " C-j żeby usuwało znak na prawo

  " Nerd
  " - quickly type directory to go
  " - shortcut to home (C-S-u)

  " W ogóle jak wychodzi z insert modea to nie robi tego od razu (są tego
  " jakieś plusy?)
  " Comment out this two lines above to fix this:

  " ESC po uzupełnianiu trzeba walnąć 2 razy (neocomplcache)

  " 0 nie wraca do początku wcięcia tylko do samego początku linijki
  " Załatwione przy pomocy <S-^> lub 0 w

  " source ~/.vimrc.20121015

  set nospell
  set go-=L

  " Bundle 'smerrill/vcl-vim-plugin'
  " VCL stuff, for editing Varnish files
  au BufRead,BufNewFile *.vcl :set ft=vcl
  au! Syntax vcl source ~/.vim/bundle/vcl-vim-plugin/syntax/vcl.vim

" }
